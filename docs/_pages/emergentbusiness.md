---
layout: single
permalink: /docs/_pages/emergentbusiness/
title: "EMERGENT BUSINESS MODELS"
excerpt: ""
header:
    image: /assets/images/

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

#feature_row:

  - image_path:
    alt:
    title: "term1"
    excerpt: "research studio"
    url: "/docs/_pages/proyecto/term1"
    btn_class: "btn--primary"
    btn_label: "Learn more"

---

##SHORT DESCRIPTION, || i.e the concept

6-hour seminar with the team of [Ideas For Change](https://www.ideasforchange.com/), Javier Creus & Valeria Righi, sharing research on impact models and methodology for designing innovations that have high impact potentials. Goals: to explore and analyse cases of impact innovations and learn to apply this approach to our own projects.

##PRESENTATION, || i.e the seminar's outcome

[First Session](https://docs.google.com/presentation/d/1pWkuueR49G0hEldyLzmofj9VXnIwrkLNVSze55yuoyI/edit?usp=sharing)

[Second Session](https://docs.google.com/presentation/d/1Tq7-pc5RzIWn7OJkTw77O-fHhARHhJkhlVgWXdqEZIE/edit?usp=sharing)

##REFLECTION, || i.e the process report

This seminar was quite helpful as it helped me realise which systems my project belongs to, see the changes that happened historically in these fields by analysing examples that fall into the same category, and finally understand and plan the future in a more effective way. For me it was really important to see also how small actions can bring bigger waves of change as sometimes we aim too big and in a generic way and miss the purpose of an intervention and for sure that costs in efficiency. Also by analysing and comparing former advances within our project's system we could see how things used to work in the ecosystem of the model, find our level of impact, how to create a value producing network in order to challenge a dominant regime, and organise an efficient future business plan minimizing internal and external efforts and yet have the best outcome possible.

In my case it was really hard to cope with these subjects as my mindset for sure does not fit in this kind of frameworks. On the other hand I have to acknowledge that it is more than necessary to be able to at least understand how things work and be in a position to describe your idea in those terms as we do live in a capitalistic era still. And it is a fact -sad but fact- that in order for an idea to become an actualised project and thrive in a world formed like that, knowledge over these factors is really important. Unless we want our ideas to be just another concept, in paper, locked in some drawer.

An easy and good way that was used to give us intel over the subjects was the pentagrowth model developed by Ideas for change. We need to keep in mind that a successfull project is one that combines disruption and benefit to the people but also profit. As the systems change and move from growth over capital to growth over value, we can project the future towards which we want to head and plan in a realistic and valuable way our pathway until then (there)

With these things in mind I formed the model for my own project, trying to foresee small changes and set goals to achieve along the way. In my model there are several steps already planned, organised per the goal I want to achieve and dissolved into different needs, according to which there are different people, organisations, etc I will address to.

#STEP 1: RAISE AWARENESS // GET ATTENTION TO THE PROJECT

>HOW: by placing mock-up booths in the city of Barcelona

>NEEDS: permission & funding (for materials)

>ADDRESS TO: the Ajuntament of BCN for permission and maybe funding // IAAC & Elisava for funding and collaboration // festivals, eg Sonar, Pride, etc for placing the booths as installations, part of their activities

In this step I am going to re-create low-budget prototypes, copies of the CONF.I.A.N.Ç.A booth designed for the final MDEF exhibition.

Suggested spots in the city are busy crossroads, eg Las Ramblas, Barceloneta, Arc de Triomf, ethnic

---

#STEP 2: MAP THE CITY // PLACE EFFECTIVE BOOTHS

>HOW: by collecting data regarding anxiety levels, creating a map using them and placing the booths at high density spots of the city

>NEEDS: data, know-how, collaborations & funding

>ADDRESS TO: people, studios, universities, organisations, companies, etc collecting, analysing and visualising data, eg Carlo Ratti, Domestic Data Streamers (short term) // neuroscientists, psychologists, sociologists researching on stress indicators (long term) // IAAC & Elisava for funding and collaboration

In this step I am going to create maps of the city of Barcelona according to the stress levels of the citizens and locate the high density points -where stress levels peak- to locate the CONF.I.A.N.Ç.A booths. For that I will need special knowledge and to collect data using different sources:

-for possible collaborations and access to the learning process / eg [MIT senseable city](http://senseable.mit.edu/) & Carlo Ratti

-for collecting existing data, from

--sensors that are already used / eg sports' clubs, NIKE, APPLE, SAMSUNG

--social media posts with relevant hastags / eg Twitter

-for creating a system to collect my own data

--through a performance or installation / eg [Domestic Data Streamers](http://domesticstreamers.com/case-studies/)

--through an open-source kit with sensors detecting hormones, heart beat, etc / eg [Smart Citizen kit](https://vimeo.com/145620646)

At this point I will search also for programs funded by the EU about cities, new technologies, etc. If there is something suitable, I will address to universities, in order to form a research team and work on the project.

---

#STEP 3: BOOTHS 2.0

>HOW: by re-designing the booths / creating the real-time communication portals / adding "human factors"

>NEEDS: know-how, collaborations & funding

>ADDRESS TO: people, studios, universities, organisations, companies, etc working on telecommunications, networks // programmers and developers // neuroscientists, psychologists, sociologists researching on trust issues, anxiety, loneliness, etc // IAAC & Elisava for funding and collaboration

In this step I am going to reform the proposed booths by adding different factors. First I would like to see a network of booths connected with each other where at the moment of confession, when data are created, an algorithm will be generated that can break down the narration (keywords and emotions), set a signal and -if it finds a matching signal- open a "call" where the two confessants can communicate real time. Then I will find a way to involve more personal approach to the concept. For example I would like to add sensors that could detect which is the most suitable time for a question (out of a set of pre-recorded questions) to pop-up to trigger the reflection of the user.

---

#STEP 4: BOOTHS 3.0

>HOW: by creating an app related to the project and re-designing the booths

>NEEDS: know-how, collaborations & funding

>ADDRESS TO: people, studios, universities, organisations, companies, etc working on telecommunications, networks // programmers and developers // neuroscientists, psychologists, sociologists researching on trust issues, anxiety, loneliness, etc // IAAC & Elisava for funding and collaboration

In this step I am going to add another factor: the customisation. For that purpose I will develop a mobile application that will be linked to the booths, which can be used at any time, at the comfort of our houses or offices. This app will work as a personal booth that stores data related to the narratives we share. When using it we will be asked also about preferences regarding our spaces, the sounds, the colours, or even the smells. With this app we will also get access to the actual booths placed throughout the city. In that way when we use the app to unlock the booth, the interior will be customised in order to make our experience more effective.

---

#STEP 5: POP-UP BOOTHS & REAL-TIME DATA

>HOW: by using the kits and apps created in previous steps

>NEEDS: know-how, collaborations & funding

>ADDRESS TO: people, studios, universities, organisations, companies, etc working on telecommunications, networks // programmers and developers // IAAC & Elisava for funding and collaboration

In this step I am going to create live-streaming maps by detecting the signals from the apps and kits, showing which area of the city is high density at which moment of the day and thus CONF.I.A.N.Ç.A is the most needed. In that way the booths can appear and disappear according to the needs. For that purpose the booths should be re-designed, probably with inflated materials and a structure that allows easy and fast construction and disconstruction.
