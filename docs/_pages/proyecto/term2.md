---
layout: single
permalink: /docs/_pages/proyecto/term2/
title: "DESIGN STUDIO"
excerpt: ""

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

  - image_path:
    alt:
    title: "presentation, March 2019"
    excerpt: "midterm presentation"
    url: "/docs/_pages/proyecto/midtermMarch/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

  - image_path:
    alt:
    title: "presentation, April 2019"
    excerpt: "final term presentation"
    url: "/docs/_pages/proyecto/finalsApril/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

---

##BRIEF, || i.e a short conclusion of the first term

A few months ago, I found it quite hard to pinpoint what was that exactly that I wanted to dive into. After a series of stimulating workshops, many mind-opening lectures by and talks with interesting innovators,  I finally came to good terms with what I was about to do. My initial idea came as an intuition, trying to answer the question "who am I and who I want to be"; I am a person interested to understand human relations and try to find ways to make them stronger. Also people tend to trust me and I tend to bond with people quite easily; and that is a feature I would like to de-code, understand how it works and re-code in order to use it in a bigger scale. Trust is a fundamental issue in any type of relationship, whether it is within oneself, a two-people relationship, or in a strategic alliance, a workspace, a community, we need to confront trust if we expect to create value.

As an architect I always wanted to find a way to facilitate how communities work, even design to develop better interpersonal and intergroup adjustments. And that for me was not about just building infrastructures; I wanted to pursue the social aspect of the architectural design and use all these skills that I attained incidentally during my tenure in the architecture school, eg problem solving, working with teams and speaking in public.

So, combining my hunch and my background load, I decided to work with trust as a tool to bond communities. The questions arose were:

-what is trust?

-how does trust work? can we measure it?

-how is technology affecting trust? can we reverse it?


##INTRO, || i.e a short preliminary part of the second term

Now that the area of interest is somehow set for me, I need to plunge into the unknown territory of trust and find my area of intervention.
To define that, I started by experimenting in the real world, trying to experience different forms of trust in order to understand them, and at the same time researching all possible aspects connected, influencing and being influenced by trust.

I have to admit it was hard, because trust is a big word; everyone understands it in a different way and even if you look for definitions, it depends on the discipline or science you take into consideration.

Challenge accepted...

---

{% include feature_row %}
