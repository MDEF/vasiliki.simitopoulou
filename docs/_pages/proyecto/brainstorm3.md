---
layout: single
permalink: /docs/_pages/proyecto/brainstorm3/
title: "INTERVENTION PROPOSALS"
excerpt: ""
header:

---

Going back to my notes, I realized that there are some #keywords that could help me frame the area of my intervention. By collecting them into post-it I tried to get from chaos to order, or at least less of a chaos. I wrote down the key aspects that troubled me the most throughout this research. I like playing with post-it; small pieces of paper that can help you map your thoughts, disassemble and re-assemble the map again and again. So if I combine the enduring struggle of the architects to provide meaningful public spaces with my foresight that we will need to overcome our differences and relate to the "unknown" pretty soon I may get down to something.

##PROPOSAL 1, || i.e gamification

One tested technique of reviving urban spaces over the past few years is called gamification. Creative initiatives offer fun remedies for our lives in the concrete jungle. Game-like experiences and interactions can kindle new urban joy on an everyday basis. With urban populations growing, we’ll need to find ways to break up the monotony of cities. Research shows that unattractive and boring architecture can have a negative impact on our psychological wellbeing. The strains of modern life are also making us feel less connected to the communities we live in. Creating a playable discussion within the urban core is essential to building more lively communities. When people play together, barriers begin to break down. It doesn’t matter what your social or economic background is or what ethnicity you are. You just become people playing, learning, and trusting one another.

One step forward for me would be to design games of trust. There is a broad literature about trust games, quite simple tricks that usually make sense once the members of a group know each other a little better or when mutual trust and confidence in each other should be encouraged. Direct feedback, reflection on the games and the experiences can be done as a conclusion. Many of these games have something to do with seeing or not seeing; it is about "blind trust". All these games are designed to trust statements and actions of other people. Trust games can be an important factor when you try to merge a group or groups. This can happen as part of the professional practice in terms of team building or education, or in small communities, and step-by-step scale up to the cities.

Another possible action would be to plan sort of a game in the public space. Instead of just existing in a public square, what if there was a springboard to do things together with other people. I picture it as "wildcards" left on strategic points or a randomizer -kind of like the wheel of fortune- which will propose different bonding activities. It is a fun and easy way to get people engaged to a cause without thinking too much; things we do "offline", unconsciously, have the greatest impact without even us realising it.

##PROPOSAL 2, || i.e randomizer

I intend to design small actions that enable bonding in order to generate a mechanism that is evolved by itself. I would like to act like a catalyzer for things to happen; for example a small dinner organised to get together unfamiliar to each other persons with diverse backgrounds and stories. How these people identify themselves before the dinner and how do they relate to people according to that perception of themselves? After the dinner what has changed? What common ground was created during it and what can be planned as a next step? *For that action I will collaborate with [Maite Villar Latasa](https://mdef.gitlab.io/maite.villar/about/)*

##PROPOSAL 3, || i.e urban devices

Last but not least, I plan to plant some urban devices to raise awareness, show what we are missing out and disrupt our daily routines. One example of that action could be the "confession points"; devices that look like simple payphones and work as fm transmitters and receivers, will be placed in key points and one can go and pick up when he feels that he needs to talk or has some time to listen. The signals will be transmitted in random places so that anonymity will be kept.

##OVERALL, || i.e experimenting & examining the results

P1: the trust games

Simple actions can reveal or empower trust. If proposed in a form of game maybe it is easier to perform them. 


---

P2: the friendly gathering

My first step was to organise a gathering of people; with *[Maite Villar Latasa](https://mdef.gitlab.io/maite.villar/about/)* we decided to ask 4 people each to come meet us on a Sunday night at her house. These 4 and 4 people did not know each other but they knew me or her, and they were willing to participate in a small "social experiment" as they somehow trusted us and wanted to help in our projects. Before arriving they did not know much; we had sent kind of a cryptic message as an invitation and they arrived full of curiosity and a bit of anxiety for the "event".


---

P3: the confession phones

---
---

##FINAL PROPOSAL, || i.e confession booth
