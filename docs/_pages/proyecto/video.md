---
layout: single
permalink: /docs/_pages/proyecto/video/
title: "MDEF VIDEO"
excerpt: ""
header:
    image: /assets/images/

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

#feature_row:

  - image_path:
    alt:
    title: "term1"
    excerpt: "research studio"
    url: "/docs/_pages/proyecto/term1"
    btn_class: "btn--primary"
    btn_label: "Learn more"

---

Promo cuts for the CONF.I.A.N.Ç.A project:

>*"what is it about society that disappoints you so much?"*

<iframe width="560" height="315" src="https://www.youtube.com/embed/y4tSA4tTBhk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

>*is it real? are you real?*

<iframe width="560" height="315" src="https://www.youtube.com/embed/WG1xBH-Ipjw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

>we leave in a world where everything around as is or could be transformed into a screen. Loads of information in the form of images reaches us everyday through them and we are so used to this condition that we do not even notice it anymore. But how does this attachment to our screens really affect us, our minds?

<iframe width="560" height="315" src="https://www.youtube.com/embed/rP_Ys_Gmtls" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

>our current lifestyle choices cause anxiety which affect every aspect of our life. We need to find ways of releasing this stress before it reached critical levels. How can we shield ourselves?

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ms8l87f2BHk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
