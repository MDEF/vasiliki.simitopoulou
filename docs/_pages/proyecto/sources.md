---
layout: single
permalink: /docs/_pages/proyecto/sources/
title: "SOURCES"
excerpt: ""
header:
    image: /assets/images/sources.gif

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

##BIBLIOGRAPHY, || i.e recommended papers, articles, books

-The Distracted Mind: Ancient Brains in a High-Tech World, by Adam Gazzaley & Larry D Rosen

-[Speculative Everything: Design, Fiction, and Social Dreaming](https://mitpress.mit.edu/books/speculative-everything), by Anthony Dunne & Fiona Raby

-[Adversarial Design](https://mitpress.mit.edu/books/adversarial-design), by Carl DiSalvo

-[Design for the Real World: Human Ecology and Social Change](https://www.amazon.com/Design-%C2%ADReal-%C2%ADWorld-%C2%ADEcology-%C2%ADSocial/dp/0897331532), by Victor Papanek

-[Liquid Modernity](https://www.amazon.com/Liquid-%C2%ADModernity-%C2%ADZygmunt-%C2%ADBauman/dp/0745624103), by Zygmunt Bauman

-[Who Owns the Future?](https://www.amazon.es/gp/product/B008J2AEY8/ref=dp-%C2%ADkindle-%C2%ADredirect?ie=UTF8&btkr=1) , by Jaron Lanier

-[This Changes Everything](https://thischangeseverything.org/book/), by Naomi Klein

-[To Save Everything, Click Here: The Folly of Technological Solutionism Paperback](https://www.amazon.com/gp/product/1610393708?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s00), by Evgeny Morozov

-[Democratizing Innovation](https://www.amazon.com/gp/product/0262720477?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s01), by Eric Von Hippel

-[Cradle to Cradle: Remaking the Way We Make Things Paperback](https://www.amazon.com/gp/product/0865475873?psc=1&redirect=true&ref_=od_aui_detailpages00), by Michael Braungart

-[Macrowikinomics: New Solutions for a Connected Planet Paperback](https://www.amazon.com/gp/product/1591844282?psc=1&redirect=true&ref_=od_aui_detailpages00), by Don Tapscott

-[The Third Industrial Revolution: How Lateral Power Is Transforming Energy, the Economy, and the World Hardcover](https://www.amazon.com/gp/product/0230115217?psc=1&redirect=true&ref_=od_aui_detailpages00g), – by Jeremy Rifkin
