---
layout: single
permalink: /docs/_pages/proyecto/midtermMarch/
title: "MIDTERM PRESENTATION"
excerpt: ""

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

##DEFINITIONS, || i.e the "formal" statements of the meaning of trust

Trust is a big, complex, multilayered word which is emotionally charged. It is a value that is placed as a central part of all human relationships, including romantic partnerships, family life, business operations, politics, and medical practices.

My first step was trying to define what trust is, searching for answers in various disciplines. There are a number of different ways we can define trust. Here are some dimensions of trust and consequent definitions.

---
![](../assets/images/proyectoterm2/march/01.png)
---

Trust is both and emotional and logical act. Emotionally, it is where you expose your vulnerabilities to people, but believing they will not take advantage of your openness. Logically, it is where you have assessed the probabilities of gain and loss, calculating expected utility based on hard performance data, and concluded that the person in question will behave in a predictable manner. In practice, trust is a bit of both. I trust you because I have experienced your trustworthiness and because I have faith in human nature.

We FEEL trust. Emotions associated with trust include companionship, friendship, love, agreement, relaxation, comfort. Behaviors and verbal expressions are certainly evidence for trust, for example when someone treats you well and says nice things to you, but these behaviors are merely evidence for the internal mental state of trust that causes them, not the trust itself. Trusting people may involve estimations of probabilities of how they will behave, but people usually trust others without any understanding of probability or any precise predictions about their behaviors. Some philosophers would say that trust is a propositional attitude, an abstract relation between an abstract self and an abstract meaning of the sentence. But the nature of these selves, relations, and meanings is utterly mysterious.

The psychological alternative that trust is a feeling of confidence and security is much more plausible than behavioral, probabilistic, and philosophical views but leaves unspecified the nature of this feeling. Emotions like trust and love are neural patterns that combine representations of the situation that the emotion is about, appraisals of the relevance of the situation to goals, perceptions of physiological changes, and (sometimes) representations of the self that is having the emotion. So we also DETERMINE(?) and RECALL trust.

Scientists and researchers claim that no matter how we define trust, there are some facts that are fundamental at any case. It takes time and a lot of "insignificant" or major shared experiences to build trust and it is a decision we make;

---
![](../assets/images/proyectoterm2/march/02.png)
---

According to Dr Brené Brown, a research professor at the University of Houston and author who spent the past two decades studying courage, vulnerability, shame, and empathy, there are seven values strictly related to trust; these 7 values form the acronym B.R.A.V.I.N.G. (in my mind it is also associated with its meaning; confronting, defying, withstanding)

---
![](../assets/images/proyectoterm2/march/03.png)
---

Throughout other scientific papers and talks about issues related to trust, there are hundreds of words mentioned as fundamentals or links to trust. For me four of the most important ones are: empathy, reciprocity, bravery and authenticity

---
![](../assets/images/proyectoterm2/march/04.png)
---

##MAPPING, || i.e trying to break down and understand what trust is for me

In an attempt to understand the "overall" of what trust is, I decided to put in a diagram what trust is for me, starting from the consensus view that everything starts and comes back to the person itself; trust is related to how our brain works, what we feel, what we have experienced etc. For that reason I place in the middle of the diagram the Person. Then, for me, there are 4 main categories of trust, regarding the parts involved: trusting your own self, another person, a certain group of people and systems. In each "category" there are several reasons of trust and by extension or by analogy mis-trust; to form the diagram I move from aspect to aspect asking "WHY", i.e why we mis-trust or trust.

Some lines in between the aspects are crossed as many of them are either effects or conditions in more than one category. Thus, the outcome is a complex "map" representing the complexity of the issue of trust

---
![](../assets/images/proyectoterm2/march/05.png)
---

So if we assume that this is my overall understanding of trust -to some extend, and there is a general assumption that trust is in crisis nowadays, for me it is important to see how we were led to that point; if there is something in our near past defining our present and future or if we can trace some patterns that can help us figure out the issue and even predict some possible futures.

Now, if we use the division by Marsall McLuhan and Alvin Toffler about societies as pre-alphabetic/alphabetic/post-alphabetic and first/second/third wave respectively and try to locate the issue of trust in each one of them, we find ourselves somewhere in between the second and the third wave societies -in limbo- where we try to make amends with the past, and at the same time move forward to the futures.

---
![](../assets/images/proyectoterm2/march/06.png)
---

My main focus is to design for a future condition, so it is urgent to understand the role of the technology in it. Taking into consideration facts and statistics, it is obvious that our attachment to the screens is going to be a major factor in our lives -even bigger than it is today.

---
![](../assets/images/proyectoterm2/march/07.png)
---

Going back to the present, as I see that technology will play a big part in our furure lives, I try to understand how trust works in relation with technology; what or who we trust when using a digital interface. There we can see an odd condition where we trust "digital things" we would not trust in our "real lives". Eg we may trust someone to sleep in his place (airbnb) we do not know in person just because we trust the intermediate while in our real life we wouldn't do it. Plus we violate the first rule of all mothers worldwide "do not get in cars with strangers" (uber). Last but not least we make "friends" using social media, which opens a huge dialogue box in the issue of trust and what we perceive as it.

It is interesting to see how we place trust in a new type of "institutions" while at the same time loosing trust in conventional systems, such as governments or religions. Without an intention to judge this decision it is obvious that there is a change in the balance and this is something to be considered to any future planning.

---
![](../assets/images/proyectoterm2/march/08.png)
---

Taking into consideration the statistics and the fact that the digital interface seems like a "user-friendly" interface, I came to the conclusion that one possible proposal would be to design a platform. This medium would work as a digital marble jar in which we would put "marbles" of trust when that occurs, mapping positive actions that are related to its development. In that way if this jar is "fed" by two or more parties can become the virtual black box of the trust relationship among these parties.

---
![](../assets/images/proyectoterm2/march/09.png)
---

This platform can have different forms. My two initial ideas were to use it as a pluggin on existing apps or as an independent app, creating something like a game to collect data about the trust relationship, or "smart" appliances on which several messages will appear as part of the "trust game".

---
![](../assets/images/proyectoterm2/march/010.png)

![](../assets/images/proyectoterm2/march/011.png)
---
