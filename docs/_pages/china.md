---
layout: single
permalink: /docs/_pages/china/
title: "Research Trip"
excerpt: ""
header:
    image: /assets/images/shenzhen00.jpg

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

#EXPLORING SHENZHEN

The research trip to Shenzhen; an experience of a lifetime. Before going I had so many expectations but all based on assumptions, of what I had heard, what I had read. The reality is far from that though. It was really interesting to be in a new-born city (just 40 years old) with 13 million citizens, the next Silicon Valley. At first everything seemed strange and far from our understanding, but in a while I realised that in fact it is not that different. Yes the technology is more advanced and embedded in the everyday life, but at the very end we are called to face the same the same addiction to it.

We all live in this technology driven era either we realize it or not. But maybe in Shenzhen it is more obvious as it is the capital of technological innovation and we see it everywhere around us –from robot cameras to interactive mirrors, and that for sure created certain expectations before going to this trip. Yet it is interesting to see how  prejudices fall apart regarding China.

Before arriving, we had this idea that people live and behave in a way so totally different than us, but the truth is there is no separating line between “us” and “them”. In fact there are more similarities than differences. For example if we observe people using public transportation, we can see more or less the same values; people absorbed in their smartphones –maybe not using the same apps but kind of a Chinese version of them.  They may be more willing to check new technologies and more familiar with the idea of change, and for sure have different approach and aesthetic, but on the other hand we see that the actual use of the technological media is not that unlike.
