---
layout: single
permalink: /docs/_pages/design2050/
title: "DESIGN FOR 2050"
excerpt: ""
header:
    image: /assets/images/

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

#feature_row:

  - image_path:
    alt:
    title: "term1"
    excerpt: "research studio"
    url: "/docs/_pages/proyecto/term1"
    btn_class: "btn--primary"
    btn_label: "Learn more"

---

##SHORT DESCRIPTION, || i.e the concept

In this 12-hours seminar, we will work in collaborative ways to design fictional narratives around the concept of internet citizenships and post-technological futures, integrating our final projects and research insights, using as a 1 billion second time horizon together with key insights from the research themes that [IAM](https://www.internetagemedia.com/labs) has been exploring in the last years, in particular from 2019’s theme: The Quantumness of Archipelagos, a ‘what if?’ remix of ideas coming from philosophy, geography, queer theory and quantum physics, shaped as an experimental thinking tool to deal with the complexity of our realities and a lens through which we can imagine alternatives, collectively.

##PRESENTATION, || i.e the seminar's outcome

<iframe src="https://player.vimeo.com/video/341754964" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

##REFLECTION, || i.e the process report
