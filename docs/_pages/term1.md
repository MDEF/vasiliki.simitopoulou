---
layout: single
permalink: /docs/_pages/term1/
title: "TERM 1"
excerpt: ""

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

    - image_path:
      alt:
      title: "week1"
      excerpt: "Bootcamp"
      url: "/docs/_pages/term1weeks/w1"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week2"
      excerpt: "Biology Zero"
      url: "/docs/_pages/term1weeks/w2"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week3"
      excerpt: "Design for the Real Digital World"
      url: "/docs/_pages/term1weeks/w3"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week4"
      excerpt: "Exploring Hybrid Profiles in Design"
      url: "/docs/_pages/term1weeks/w4"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week5"
      excerpt: "Navigating Uncertainty"
      url: "/docs/_pages/term1weeks/w6/5"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week6"
      excerpt: "Designing with Extended Intelligence"
      url: "/docs/_pages/term1weeks/w6"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week7"
      excerpt: "The way things work"
      url: "/docs/_pages/term1weeks/w7"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week8"
      excerpt: "Living with Ideas"
      url: "/docs/_pages/term1weeks/w8"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week9"
      excerpt: "Engaging Narratives"
      url: "/docs/_pages/term1weeks/w6/9"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week10"
      excerpt: "An Introduction to Futures"
      url: "/docs/_pages/term1weeks/w10"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week11"
      excerpt: "From Bits to Atoms"
      url: "/docs/_pages/term1weeks/w11"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week12"
      excerpt: "Design Dialogues"
      url: "/docs/_pages/term1weeks/w12"
      btn_class: "btn--primary"
      btn_label: "Learn more"

---

##SHORT DESCRIPTION, || i.e the concept

The first term is developed throughout 12 weeks, each one of which is an individual module part of the 4 main lines of the master (application, reflection, instrumentation and exploration)

APPLICATION – Research studio. Establishing an area of interest, analysis and research. Definition of topic and direction of the final project.

REFLECTION – Design identities and methods. A day in the life of hybrid designers from Barcelona. Building knowledge, skills and attitude as a designer

INSTRUMENTATION – Introduction to digital fabrication. Distributed manufacturing and Industry 4.0

EXPLORATION – Data and information in the digital age. Communications, computation, change theory

##12 SEMINARS, || i.e introductory courses, theoretical background.

---

      {% include feature_row %}
