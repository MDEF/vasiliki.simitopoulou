---
layout: single
permalink: /docs/_pages/designNEW/
title: "DESIGN FOR THE NEW"
excerpt: ""
header:
    image: /assets/images/designnew.gif

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

##SHORT DESCRIPTION, || i.e the concept

The goals of this seminar are:

Understanding and reflecting on the implications of the performativity of everyday life in the creation of new normalities from a mind-body perspective and the role that design plays in that context.

Designing paths of transition from the present (and its practices) to the desired scenarios through strategic interventions in the metaphorical, material and practical spheres through the construction of speculative prototypes (material, performative, narrative…)

Engaging in a process to iterate, concretize and refine students’ projects integrating the learnings of Transition Design and the work on social practices.

##DOCUMENTATION, || i.e not sorted notes taken from the lectures

3days of lectures, activities and homework:

#Day 1: Approaching the practice

[Uninvited Guests](https://vimeo.com/128873380) is a short film that explores the frictions between an elderly man and his smart home

[Chindogu](https://www.chindogu.com/) is something not exactly useful, but somehow not altogether useless

"Awareness of change is what distinguishes design fiction from jokes about technology, such as over-complex Heath Robinson machines or Japanese chindogu (weird tool) obj. Design finction attacks the status quo and suggests clear ways in which life might become different", Bruce Sterling

-How do we materialize our emergent future?

-What is the everyday practice?

-Which is the right pathway to get there?

**Intervention dimension: artifact or experience?**

Artifacts:

Trigger reflection

The cyborg manifesto by Donna Haraway

Immersive scenarios:

Situation Lab @futuryst

Transmedia Design

“Transmedia Design, Transmedia storytelling represents a process where integral elements of a fiction get dispersed systematically across multiple delivery channels for the purpose of creating a unified and coordinated entertainment experience. Ideally, each medium makes it own unique contribution to the unfolding of the story.” Henry Jenkins definition of Transmedia Storytelling

[“The Zone of Hope 1917-2017”]() by Data Streamers

“L’ alegria” by Marilia Samper

“Hyperreality is more real than reality” Baudrillard

It’s a simulation of reality that people perceive as real. A Pre-totype creates a state of hyperreality, locating a New Normal (a possible
near future) into the present, so people can explore it, think about it and make value judgments about it.

Design helps improve reality

GLOBAL CHALLENGES

socio-economic

climate

Dissemination and impact

-When faced with an hyperreal pre-totype, people have to deal with it and with its implications.

-At this point discussions and emulations will start.

-New models will be discussed and re-invented.

-Critique and appraisals will take place.

-All the feedbacks aroused from our Pre-totypes are collected, whether they happened online, in cities, in rural areas, in conferences or somewhere.

-Finally, the collection of these reactions is made available, so that everybody can use it to extend the discussion, going beyond the limit of understanding what the possible futures are, but also discussing what the preferable and desirable ones are, and for which communities, organizations and individuals.

“A monad is not a part of a whole, but a point of view on all the other entities taken severally and not as a totality"

“to ‘interact’ with one another: they are one another, or, better, they own one another to begin with, since every item listed to define one entity might also be an item in the list defining another agent (Tarde 1903; 1999 [1895]). In other words, association is not what happens after individuals have been defined with few properties, but what characterize entities in the first place (Dewey 1927). It is even possible to argue that the very notion of ‘interaction’ as an occasional encounter among separated agents is a consequence of limited information on the
attributes defining the individuals (Latour 2010).”

![](../designnew/assets/images/01.jpg)

![](../designnew/assets/images/02.jpg)

![](../designnew/assets/images/03.jpg)

[TRANSITION DESIGN](https://transitiondesignseminarcmu.net/) brings together new knowledge and skillsets aimed at seeding and catalyzing systems-level change. The framework is comprised of four mutually influencing, co-evolving areas of knowledge and skillsets:

>VISION, THEORIES of CHANGE, NEW WAYS of DESIGNING, MINDSET & POSTURE

![](../designnew/assets/images/04.jpg)
![](../designnew/assets/images/05.jpg)

**The everydayness of a future**

Object oriented ontology, Human centered design, Designing practices

![](../designnew/assets/images/06.jpg)

‘A routinized type of behaviour which consists of several elements, interconnected to one other: forms of bodily activities, forms of mental
activities, ‘things’ and their use, a background knowledge in the form of understanding, know- how, states of emotion and motivational knowledge.’
(Reckwitz 2002a:249)

>Taking  a shower

(Shove and Pantzar) PRACTICES:

**Stuff**, refers to the tangible, material elements deployed in the practice. summarize them as objects, infrastructures, tools, hardware and the body itself.

**Skills**, are learned bodily and mental routines, including know-how, levels of competence and ways of feeling and doing. The important point
here is that in this approach, ways of feeling about and appreciating things and situations is seen as part of the practice, as learned through doing.

**Images**, are socially shared ideas or concepts associated with the practice that give meaning to it; reasons to engage in it, reasons what it is for, or, ‘the social and symbolic significance of participation at any one moment’

**Links**, since ‘practices emerge, persist and disappear as links between their defining elements are made and broken’ these links are important for understanding change in practices

[DOLLAR STREET](https://www.gapminder.org/dollar-street/matrix)

NEED vs TARGET

**MAPPING**

the actor-network: How do they relate to each other?

related social practices: Which social practices can you identify from the map we have built?

locating intervention: Where does your interventions lie in the map? Which are the main disruptions that your intervention is suggesting?

**DECONSTRUCTING**

analyse practice, narrate, build in skills/images/stuff

**APPROACHING THE PRACTICE**

What patterns emerge when you compare the elements and links of all these ways to go about this practice?

What historical trends do you see when you compare the elements and links of all these ways to go about this practice?

What is influencing indicators in undesired ways when you compare the elements and links of all these ways to go about this practice?

What opportunities or spaces for a design intervention do you see emerging?

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#Day 2: Designing the transition pathway

If we have our practice map, can we track the historical progress that this practice has suffered?

What is today’s mainstream way of going about this practice?

What alternative ways of going about this practice exist in my society?

What alternative ways of going about this practice exist in other countries?

What alternative ways of going about this practices have existed in history?

WHAT IS THE FUTURE CONE of our practice?

![](../designnew/assets/images/07.jpg)

Starting from today, how can we reach our future vision? which are the steps we should take in order to succeed our goal?

![](../designnew/assets/images/08.jpg)

**CURRENT PRACTICE vs DESIRED PRACTICE**

How did the process unfold?

Which are the main differences between CP and DP?

Which were your main learnings?

**TRANSITION PATHWAYS**

![](../designnew/assets/images/09.jpg)

Find leverage points

>practices / foresight / intentions

Co-created, long-term visions serve as both magnets drawing stakeholders into that future, and a compass by which to steer near and mid term projects

![](../designnew/assets/images/010.jpg)

Instead of thinking in terms of one-off solutions, that are completed within relatively short time frames, TRANSITION DESIGN thinks in terms of designing INTERVENTIONS that are implemented at multiple levels of SCALE, over short, mid and long TIME HORIZONS

**ACUPUNTURA DE SISTEMAS**

>REFRAMING PRESENT & FUTURE / DESIGNING INTERVENTIONS / WAITING & OBSERVING / REFRAMING ...

**Spatio-temporal matrix**

![](../designnew/assets/images/011.jpg)

MACRO PAST: large, historical events, situations or trends that contributed to the problem / what was the society like before the problem arose?

PAST: what are the historical roots of the problem? / when did it first begin? how were people living prior to the emergence of the problem? / what had they been doing to prevent it from arising?

MICRO PAST: everyday life and practices in the past that may have contributed to the problem? / what practices in very distant past prevented the problem from arising?

---

MACRO PRESENT: current, large scale events, situations or trends that contributed to the problem / what is happening at this level that might open a possibility for interventions at lower levels?

MICRO PRESENT: specifics of everyday life and individual practices that contribute to the problem / what interventions at this level scale could help resolve the problem?

---

MACRO FUTURE: what would life be like at this higher level of scale if the problem were resolved? / what positive, large scale events would have taken place? / how might social norms have changed?

MICRO FUTURE: how would everyday life and its practices in the future be different if the problem were resolved? / what would be possible that currently is not? / how would practices at home, work, play etc have changed?

**How does transition happen?**

1. experience the need for change

2. diagnose the system

3. create pioneering practices

4. enable the tipping point

5. sustain the transition

6. se the rules of the new mainstream

**evolution of the practice**

investigate, analyse, find all key aspects

**Tools for pivoting social practices**

![](../designnew/assets/images/012.jpg)

new images >> building new narratives

new skills >> constructionism (Seymour Papert): "holds that learning can happen most effectively when people are active in making tangible
objects in the real world. You cannot separate learning from experience”

new stuff >> (this is our habitual battleground as designers)

eg [Curious Rituals](https://curiousrituals.wordpress.com/), Gestural Interaction in the Digital Everyday

We tend to overestimate the technological influence on creating new practices

**Measure the intention**

PROBLEM <> SOLUTION

PRACTICE as a constellation of images, skills and stuff and a certain average level of resource consumption per performance

Where we are observing more change?

How does transition happen?

What we will do today?

Where to most effectively influence?

PEOPLE <> SYSTEM

The position of practice theory within social theory (Reckwitz)

EVERYDAYNESS

Diagnose the system by embodying it

**4D Sculptures**

3 roles:

-Sculpture: Embody the one element of the practice. Body and others awareness

-Note takers: Captures everything that is said

-Space holders: Outside system observers, without judgment.

2 images:

-Present Practice

-Future targeted practice

Transition and guided conversation:

-Listening us

-Listening the system

-Listening the change

HOW CAN WE CHANGE?

How can we intervene?

What needs every element of the system to move forward in the change?

HW: Diagnose the transition pathway of your practice / Identify and prioritise ideas, strategies and tools for pivoting your practices / Identify how can this ideas can enrich, improve or complement your intervention.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#Day 3: Materializing the intervention

Pushing an idea to the future

Practices: ‘A routinized type of behaviour which consists of several elements, interconnected to one other: forms of bodily activities, forms of mental activities, ‘things’ and their use, a background knowledge in the form of understanding, know- how, states of emotion and motivational knowledge.’ (Reckwitz 2002a:249)

The practices of my everyday

My project and the practices that will touch

We have focused in understanding one of these practices affected and transformed by your vision of the future

Which interventions we can do to transform the current practice?

You can intervene the objects, the images, the skills, the links

Choose interventions / Prototype interventions / Rehearse, learn and improve interventions / Document the rehearsals for the derivables

**Question exploration**

Which things would you like to understand more about the situation?

Which elements would you like to shine some light into?

Which uncertainty areas do you identify from your intervention?

Whose reaction or behaviour is unknown by you?

Which relationships could entail more conflict?

Which different actants (animate and non-animate) interact in your intervention?

Which elements you would like to rehearse or prototype before going to build the intervention itself?

**Starting point**

Characters : Both animate and non-animate / Which actants may interact in your situations? / Which are their goals and jobs to be done? / Which kind of needs they would like to have met? / Which are their personality traits? / Which of you could embody each of the different characters?

Props (needed objects) : Which elements will populate your intervention? / With which elements may your characters interact? / What’s the reason to be of those elements? / Which are the capacities that that those elements extend for the users?

Set-up and scenery : Places, temporalities, space, atmosphere.

Script and conflict : Narrative arc / main conflicts

##REFLECTION, || i.e the process report

During this 3-day workshop we tried to shaken up a little bit our personal perspectives over our projects and talk in different terms about them.

First step was to locate our desired practice, by creating a map of the network of actors related to our area of interest. Then on the same map we placed social practices related to the actors and then we projected our interventions as one practice. The first day ended by trying to collectively deconstruct one practice.

Since our practice was somehow defined, we diagnosed the transition pathway of it, identified and prioritised ideas, strategies and tools for pivoting our practices. We searched for the past and present of this practice and projected it in the future. Starting from the point of what circumstances led to the present situation that we want to change and relating it with what "turn" we want our future to get -led by our intervention- we found ideas that can improve or complement our initial design action.

#BITACORA, describing the learning process

My main concern is how we are behaving (alone and towards others) in a certain (public) space, so I tried to dive into practices related to that. To find my point of entry I mapped the actors contributing to the subject of collectiveness (which was the general title of our group of interest). I tried to seperate the actors in active members-people & groups, actions & situations, places & conditions.

![](../designnew/assets/images/013.jpg)

Then I focused on activities, practices, space related, that can give to an open or public space the character of shared or common space. There is a fine line between these two, as in a shared space the interaction is not a always a condition; eg we may sit on the same bench, thus we are sharing it, but we are both on our phones so we have no interaction with each other.

![](../designnew/assets/images/014.jpg)

My intervention's goal is to change the way of being in a public space and as a result the way of connecting with others. If we zoom in that area, then we can see more clearly which factors affect the most the present conditions, the recent past and project the future.

![](../designnew/assets/images/015.jpg)

>PRESENT :

![](../designnew/assets/images/016.jpg)

![](../designnew/assets/images/017.jpg)

When I try to deconstruct the current situation -by building different scenarios in my mind, picturing everydayness in public space- in the triangle of skills/images/stuff this is more or less what we get:

![](../designnew/assets/images/018.jpg)

>PAST :

Were things like that in the past? Most of the stuff at least where different... But the images were common. At least regarding the recent past. I chose two stations in history to see the conditions:

![](../designnew/assets/images/019.jpg)

>FUTURE :

![](../designnew/assets/images/020.jpg)

You can navigate through the map [here](https://prezi.com/view/Ag7sF2W6X9LvoyezDywi/)

---

#INTERVENTION, transition pathway

The transition pathway step by step -regarding my personal intersts. My intervention contributes in the change of our everydayness, the way we exist, behave, feel and relate in public space; on our way to work, when walking back home, when we go for night walk in the city, at any given moment

>Experience the need for change;

I've been living in cities my whole life. I am not even sure I know how to live away from one. I was lucky enough to experience all different scales, from small cities in Greece to metropolis like Toronto or Hong Kong,  and for various amount of time, from short-period stay to years. Maybe the feeling is not exactly the same in each one of them but the fact that people tend to avoid human contact and try to find "shelter" in their black screens and headphones cannot be argued.

>Diagnose the system;

Despite the fact that we become more and more addicted to our smartphones and spend hours lost in their "space", there is an additional reason that nowadays more than ever we feel that we do not quite belong in the public space; and that is because of the way it designed. We feel exposed and it is relatively easy to put the smartphone "shield" on. In cities that are never silent, it is difficult to locate yourself, find a point of stillness where you can just relax. This fact puts an extra weight on our stressful modern lifestyles and if we count the limited "free time" we anyway have, we need even more this effortless moment of silence. That is why yoga practices and meditation techniques become more and more popular over the past decades.

For me though, we should not neglect that finding ourselves does not mean isolation from others; on the contrary we may find comfort in someone else's words that no guru can give us. The bet here is, can we relate to people in a different way that we already do? For sure social media brought new horizons that we didn't even think existed, but at what cost? We become mobile engaged, we forget how to connect in the real world. We assume we know someone because of some photos and texts, but do we really know his/hers thoughts and stories?  

>Create pioneering practices;

So if we have a spot in the public space where we can just lay down and relax for a moment, away from city's noise and push notification, how is it going to affect our lives? And if we could even speak our minds without anyone commenting, "liking", judging, what would we share? If whatever we share becomes part of a chain of stories, would it provide to comfort to someone completely stranger?

In a way in that secure cocoon not only we are empowered to let go of what we carry but also, with that action, we connect with others' feelings through their stories.

>Enable tipping point;

The starting point for me would be to place some sample boxes in squares or crosspoints were thousands of people go by everyday and invite them to experience the mindfulness and connection that can be brought to them through this environment. I picture them as sort of confession booths were no one -not even God- is judging and anyone is welcomed to express himself freely, and by doing so, adding a story to the circle of stories shared within the network of booths.

>Sustain the transition;

To make the system more effective and find the spots in the cities where it would be more useful to place booths, I have certain steps in mind. First I would like to see our cities mapped using data regarding stress levels; specially designed sensors that count hormones and heart beats and scrapping through tweets that indicate anxiety can give us valuable data in order to read our cities in an alternative way. Then high and low density points would be valued in a different way and the booths could be placed accordingly.

>Set the rules for the new mainstream;

Then each booth can be an active portal of communication; something like professor Xavier's Cerebro room, where he could locate any mutant. Imagine will narrating your story an algorithm "reading" in between the lines and connect you with someone that faces similar situations. A machine that can detect emotions and keywords related to them would be able to analyse the story shared, break it down, and find similar keypoints with another story shared at the same time at another booth in the city.

#STORYBOARD, a simple storyline

A regular day for M & P, two citizens of Metropolis in the year 2050:

![](../designnew/assets/images/021.png)
