---
layout: single
permalink: /docs/_pages/fabacademy/
title: "FABACADEMY"
excerpt: ""
header:
    overlay_image: /assets/images/fabacademy.gif

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

    - image_path:
      alt:
      title: "week1"
      excerpt: "Project Management"
      url: "/docs/_pages/fabweeks/w1"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week2"
      excerpt: "Computer-aided Design"
      url: "/docs/_pages/fabweeks/w2"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:   
      title: "week3"
      excerpt: "Computer-controlled Cutting"
      url: "/docs/_pages/fabweeks/w3"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week4"
      excerpt: "Electronics Production"
      url: "/docs/_pages/fabweeks/w4"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week5"
      excerpt: "3D Scanning and Printing"
      url: "/docs/_pages/fabweeks/w5"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week6"
      excerpt: "Electronics Design"
      url: "/docs/_pages/fabweeks/w6"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week7"
      excerpt: "Computer-controlled Machining"
      url: "/docs/_pages/fabweeks/w7"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:   
      title: "week8"
      excerpt: "Embedded Programming"
      url: "/docs/_pages/fabweeks/w8"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week9"
      excerpt: "Molding and Casting"
      url: "/docs/_pages/fabweeks/w9"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week10"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w10"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week11"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w11"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week12"
      excerpt: "Applications and Implementations"
      url: "/docs/_pages/fabweeks/w12"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:   
      title: "week13"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w13"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week14"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w14"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week15"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w15"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week16"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w16"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week17"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w17"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:   
      title: "week18"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w18"
      btn_class: "btn--primary"
      btn_label: "Learn more"

    - image_path:
      alt:
      title: "week19"
      excerpt: ""
      url: "/docs/_pages/fabweeks/w19"
      btn_class: "btn--primary"
      btn_label: "Learn more"

---

##SHORT DESCRIPTION, || i.e the concept

FabAcademy is a fast paced, hands-on learning experience where students learn rapid-prototyping by planning and executing a new project each week, resulting in a personal portfolio of technical accomplishments.

##19 WEEKS, 19 ASSIGNMENTS, || i.e how to do almost anything

---

      {% include feature_row %}
