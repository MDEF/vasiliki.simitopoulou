---
layout: single
permalink: /docs/_pages/atlas/final
title: "Atlas of Weak Signals, Final Submission"
excerpt: ""
header:
    image: /assets/images/atlasfinal.jpg
last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

##REVIEWING THE WEAK SIGNALS, || i.e the 25 trends from my viewpoint

Well that was intense; in just 15 hours we navigated through facts which shape our present and discussed how possible vectors could form our future. In an effort to analyse, map and understand them, we had group discussions and assignments projecting each vector in a future scenario. It was interesting to see where the so-called crisis-es of our times could actually be turned into entry points for bringing changes and alter the direction of our stories. Identifying the weak signals -or black swans, or wildcards- is not an easy task; you don’t know how to predict them as you don't know it is going to be there until it is there. So we can only assume that something is more likely to happen, taking into consideration the near past and present conditions. But also we should consider what is happening out there, beyond our personal, national or even human bubble; ever heard of the butterfly effect? exactly that! a small seemingly insignificant change can cause effects on our whole perspective of the future.

Maybe soon we can detect new weak signals, indicators for great transformations or triggers for major shifts in the flow of history. But for now it would be enough to recognize and evaluate the 25 given ones and find the points of intersection where our personal projects can be relevant and apply in practice. It is nice to see that what you think and design is not just another piece of fiction but can be implemented as there is actual need for it. And on the other hand it is intriguing to realise the importance of some events and facts of our times.

Setting the frame of the present, the discussion started -as expected- by discussing life in "THE TIMES OF SURVEILLANCE CAPITALISM". We know that our lives are somehow held captive by the World Wide Web, its companies and its applications; we cannot even imagine a day without social media, although we know that we are constantly feeding them with our personal data and we get back fake stimulants and overload of information. We become more and more isolated and self-centered as we feel the comfort of the eco chambers that we create by choice, but get magnified by the algorithms of the media we use. And outside the WWW -in the "real" world, what is our role? are we active citizens of this planet or just consumers of literally EVERYTHING. We consume things we fabricate -that we do not necessarily need- but also we consume nature.

And that brings as to the second session, talking about the impact of the the acts of the "ANTROPOCENE". We think we know the consequences of our actions regarding the environment, but is that so? We like to place the blame on the big industries and this may be partially accurate, but truth is we start messing up long before the industrial revolution; around 12 thousand years ago when we settled down, and from hunters became farmers; when the agriculture appeared. Of course 150 years ago things started accelerating as the workforce developed artificially and we began to employ natural resources exponentially. And by we, I am referring to the first world. Although we may have a different level of responsibility towards the problem as individuals, for sure we share the same proportion of guilt as the industrially advanced world. And not only because of the consumption of coal or the emmitions of CO2, but also as our societies feed the capitalistic beast. For some scientist we should even consider talking about the effects of the "CAPITALOCENE" and not the anthropocene, as it is a fact that not the act of every single human is equally affecting the line of impact. But a man has to work right? And it is impossible to leave no carbon footprint...

Maybe not. Can we picture a future where our current jobs will be replaced by carbon neutral AI machines? How is our life going to be "AFTER AI AND THE END OF JOB"? We are mostly defined by our occupation and in a way it is what makes us competitive, keeps us on tracks and shifts us out of our laziness. Be replaced by a robot may sound scary, but at the same time promising. It may be able to do the job better than us, plus we get to enjoy all the free time of the world. And again this sounds scary and promising at the very same time. It is a fact that AI already can handle issues more effectively than a human being, but what about the bias or the ethics? And let's say that we can find a way to overcome these obstacles, what will happen with the new "leissure class"? Back in the 1899, the economist Thorstein Veblen, in his treatise "The Theory of the Leisure Class", states a social critique of conspicuous consumption, as a function of social class and of consumerism, derived from the social stratification of people and the division of labor, which are the social institutions of the feudal period (9th–15th centuries) that have continued to the modern era; Veblen talks about a privileged wealthy elite who own the means of production, have employed themselves in the economically UNPRODUCTIVE practices of conspicuous consumption and conspicuous leisure, which are useless activities that contribute neither to the economy nor to the material production of the useful goods and services required for the functioning of society, while it is the middle class and the working class who are usefully employed in the industrialised, productive occupations that support the whole of society.

In a future where the means of production are autonomous systems, "owned" by spoiled priviledged, there are issues arising even more urgent. For example, ethical dilemmas far beyond whether a driverless car do when it needs to choose between hurting its passenger and hurting a pedestrian, would be in the center of discussion. Furthermore, what does this new era mean for the societies? do social classes still exist, and in what form? Can technology bring equality and new job opportunities or we just have to find ways to spare our time productively, avoiding becoming coach potatoes? Maybe if we succeed to build, understand and co-operate with machines that exceed all the aspect of human intelligence. But then if the machines are equals to humans, what does that mean for the human identity?

But what is our identity anyway? We are commonly identified by our nationality, by the flag on our passport. Though in the not-so-recent years this started not making that much sense. In an "AFTER THE NATION-STATE" era what does our passport really mean? Well for me it is a paper that can tell you how easily -or not- depending on you origin country, you can move around the globe. Because it is a fact! People flee; we are nomads again. On the one hand all of us, the first world citizens who in the pursuit of better education, career of simple lifestyle, we move away from our mother countries and become "citizens of the world". On the other hand political turbulence in many regions of the world has increased the number of displaced people fleeing complex emergencies and disasters. Forcibly displaced people include internally displaced people (people who remain in their own countries) as well as refugees (people who cross international borders) and according to UNHCR the number of these people globally is at an all-time high since the end of World War Two. This year’s Global Trends report shows that the world’s refugee population stood at 25.4 million at the end of 2017, 19.9 million of which fall under the mandate of UNHCR.

Refugee crisis; being from Greece I am more than familiar with it. People moving by thousands, often ending up in large camps to find "temporary" settlements. But there is nothing temporary in these situations. People are trapped in this limbo condition for months -if they are lucky- even years. And the camps become slowly but steadily the new cities. We used to say the cities of tomorrow but it is happening; right here, right now. Ephemeral -in theory- cities whose inhabitants have been placed there like pieces in a puzzle. Stand-by cities with no natural origin or evolution, not official recognition, that architecture has not embraced. But a refugee camp is indeed a city, just a barely livable. Running water and the disposal of waste are not the only requisites. Being a refugee is far beyond a state now; it is an identity, a nation of its own, with necessities and rights. Being an architect, I was always concerned about the influence of architecture on people's quality of life; so where do architects stand regarding the refugee crisis? with what kind of tech can we equip people, what infrastructures are needed in the "new cities"?  How can we ensure the integration of populations?

Integration; big word. Social integration is the process during which newcomers or minorities are incorporated into the social structure of the host society. Social integration, together with economic integration and identity integration, are three main dimensions of a newcomers' experiences in the society that is receiving them. A higher extent of social integration contributes to a closer social distance between groups and more consistent values and practices. Bringing together various ethnic groups irrespective of language, caste, creed, etc., without losing one's identity. It gives access to all areas of community life and eliminates segregation. Personally I think this is one of the biggest struggle of our times. Social integration does not mean forced assimilation. Social integration is focused on the need to move toward a safe, stable and just society by mending conditions of social disintegration, social exclusion, social fragmentation, exclusion and polarization, and by expanding and strengthening conditions of social integration towards peaceful social relations of coexistence, collaboration and cohesion.

In my personal perspective social integration can never be achieved if we do not put effort in understanding and relating with what or who is not us. It is relatively easy to find common ground with people we think we are similar, but the challenge is to accept what's different. Identity once again is the core. We are men and women, and non-binary; heterosexual and homosexual, and amphisexual; young and old, and non-ageing; white and black (really is it even a thing still? I thought we left apartheid formally back in the early 90s; but I guess I am wrong); Christians and Muslims, and atheists; Is there a place for all of us? of course yes! We need to understand our body, our mind, what shapes our beliefs, and pay respect to all the others out there that find their identity in something that is not ours. We should remember that the future is not going to be heteropatriarcal, nor white, nor western; thank God -if he exists..

---
---

##CREATING THE ATLAS, || i.e learning the tools

It is a complex and big world we live in, and mapping all the events and trends, and their subtopics, even using just keywords is a messy situation. But it is crucial to visualise the context in order to understand how the different signals are related and where our personal interests fall in this atlas of coherent tendencies.

Thankfully there is a useful tool we can develop to help us with this task. First things first; organisation.. To systematise the references related to each topic we created a spreadsheet with links and notes; the content of what the topic is about, found in different different sources. Then as these sources were in digital platforms, we to apply the method of DATA SCRAPPING. We automated a process by using a web crawler, which facilitated and copied specific data from the platforms. Our goal was to extract the main keywords from each article. To process with this we used an open spreadsheet with Neuro-Linguistic Programming/NLP that is based in algorithms for Text Analysis, Machine Learning, Computer Vision and Deep Learning. So now not only we have a series of links of sources related to each topic, but also the main keywords that exist in them, and thus we can see which are being repeated, observe the patterns created in between the topics and re-organise the topics according to them. By getting into more and more detailed keywords we can disassemble and re-assemble the signals in a way that make sense to us and create our atlas of the future.

Now what's left is to visualise that data we have gathered. Charles S. Peirce introduced the term semiosis (etymology from Greek sēmeiōsis: observation of signs; from sēmeion: sign, point) as an action, or influence, which is, or involves, a cooperation of three subjects, such as a sign, its object, and its interpretant, this trirelative influence not being in any way resolvable into actions between pairs” This specific type of triadic relation is fundamental to Peirce’s understanding of “logic as formal semiotic”. By “logic” he meant philosophical logic. He eventually divided (philosophical) logic, or formal semiotics, into speculative grammar,(…) how signs can signify and, in relation to that, what kinds of signs, objects, and interpretants there are, how signs combine, and how some signs embody or incorporate others. (Semiotic theory of Charles Sanders Peirce – 1860)

So if we take each term and link it according to the semiotic triangle of concept-object-symbol and then find were these fall in relation to other terms' "triangles", we get to construct a map containing the information obtained in a visual representation. E.g this is a sample map regarding the weak signal of longtermism

---
![](../atlas/assets/images/longtermism.jpg)
---

##FUTURE OF THE CITIES, || i.e the assignment

So, in between all these crossing wildcard scenarios, how are the cities of the future going to be? Which of these signals affect directly or circumlocutorily the way we live, behave, connect, experience a city? In the year 2050, what will it mean to be a citizen?

Trying to imagine that, we created the following project, picturing a city (Barcelona) in the year 2050, where the symbol of capitalism and urban regeneration "solution" of our times -the mall- is being reformed; the citizens from simple consumers become active members of the society, being brought back to the role of the citizen in the ancient greek "polis", where they were well informed of the responsibilities that come with the right of being called citizen. Therefore we project a new type of collectiveness, structured and funded by the governance but brought into life by the civilians. In the "new malls", the citizens of Barcelona2050 are dedicated to "give in order to get back"; they offer their capabilities to the community and receive the access to public services.

---
![](../atlas/assets/images/futurecities1.jpg)

![](../atlas/assets/images/futurecities2.jpg)

![](../atlas/assets/images/futurecities3.jpg)

![](../atlas/assets/images/futurecities4.jpg)

![](../atlas/assets/images/futurecities5.jpg)

![](../atlas/assets/images/futurecities6.jpg)

![](../atlas/assets/images/futurecities7.jpg)

![](../atlas/assets/images/futurecities8.jpg)

![](../atlas/assets/images/futurecities9.jpg)

![](../atlas/assets/images/futurecities10.jpg)

![](../atlas/assets/images/futurecities11.jpg)

![](../atlas/assets/images/futurecities12.jpg)

![](../atlas/assets/images/futurecities13.jpg)
---

##DESIGNER OF EMERGENT FUTURES, || i.e implementing atlas

My mind is like a web browser right now; 25 open windows with hundreds of tabs each. I don't even know where to start, which signals I can exclude, how to construct the weak signal map of my personal project. So let's take it the other way round. How does the future in which this project is applied look like? How is the society form, what it means for the people and how can we get there?

Trust...the fundamental of all relations. Is 2050 better? I don't know. But for sure it is different. First and foremost the technology is evolved and as a result our relation with it and the way it mediates our relations with each other. Second, the number of people displaced either willingly or forcibly skyrocketed and the transformed cities are challenged by the dynamics created by the newly added groups. In my personal perspective these two main axes will drive the changes in the societies not long from now. -Of course there is the climate crisis, but thankfully things will be in a positive orbit by then.

So if we picture the societies of 2050 even more diverse than they are today and with no "common ground", experiences, memories, to base and build upon their relations, there should be found a way to enable trust despite this lack. And technology should be an ally. If AI has already reached the levels we assume today it can -or even exceeded these limits- then we should make sure beforehand that we have found a way to build AI on a non-bias stable base; media used should be redesigned in a way to protect human relations and use personal data in more efficient ways and not just to promote consumerism. And for sure this new tech should be accessible to all, equally. And this may seem an opening for new perspectives over what education and occupation is.

So I guess I should pay more attention to the following weak signals: technology for equality, human-machine creative collaboration, fighting AI data, dismantling filter bubbles, redesigning the social media, attention protection, circular data economy, refugee tech, new jobs, pick your own passport.

I decided long ago to focus on the one-to-one trust as it seems that it is what defines the other relationships as well and see how technology is affecting this type of trust as we live in the 3rd wave of change and our lives and relationships are mediated by technological media. To understand how our lives are going to be influenced even more in the future, we should only mentioned that it is calculated that in the next 40 years, we will spend 520 days watching TV series, 02 years sending messages, 03 years on social media, 06 years watching TV, 08 years online, 10 years in total starring at screens. To fully comprehend the size of the issue, I'll just say that in a typical lifetime we spend 06 years dreaming. We check our smartphones approximately 150 times a day, which is translated in once every 6 to 7 minutes while being awake.

How does that affect our brain? How is the media designed to "trick" our human mortal brain, and get it addicted to them? Can we de-code how it works and re-create a system with the right affordances to enhance values such trust? If we are going to stare at screens more than even sleeping in the coming years, at least can we make this time count? Instead of using apps that are addictive and designed to isolate us, what if there was a way to use technology to our advantage and  get the help needed to decrypt and build trust?

To conclude, I propose that a possible point of intervention could be formed by locating the relation of all the previously stated signals with the human brain. Understand the connection they have with the function of the human brain and design affordances, triggers and mechanisms, that will empower trust beyond the limitations of familiarity, common (hi)stories, etc.
