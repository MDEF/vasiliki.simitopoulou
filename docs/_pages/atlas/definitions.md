---
layout: single
permalink: /docs/_pages/atlas/definitions
title: "Atlas of Weak Signals, Definitions"
excerpt: ""
header:
    image: /assets/images/atlasdefinitions.png

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

##SHORT DESCRIPTION, || i.e the concept

This part of the course is about detecting “weak signals” that set trends and point to certain directions, based on the analysis of the main change factors we can detect in the present. Any cartography we use for understanding the present requires an analysis of the main crises that determine our collective future. Towards the end of the 21st century, these include at least: an ecological crisis, a crisis of the economic regime, multiple crises of sovereignty and representation, a crisis of the discourses of the most recent utopia, a crisis of the productive model and the nature of work in the face of the growing development of automation and robotization and A.I, a crisis of the cultural and social hegemony of privileged groups that are overrepresented in politics, culture or business and a migratory crisis of those who escape from all other crises.

These vectors, and some others, define the territory in which we build our collective projects and our hopes for collective development. What does it mean to live and coexist in these conditions of departure? How do we design for these times that are necessarily cyborg and for the anthropocene? What narratives, images, symbols and aesthetics help us find and recognize ourselves in this space?

From these 9 vectors, this seminar presents stories, narratives, proposals and images that allow the construction of an Atlas of Weak Signs for the design of Futures. Terms as surveillance capitalism, attention economy, data extractivism, filter bubbles, capitalocene, geoengineering, long-termism, etc, are being analysed and developed throughout the -5 sessions x 3 hours- seminar.

##DOCUMENTATION, || i.e not sorted notes taken from the lectures

WEEK 1;

the future cone, how we think the future

3 possibilities
Trend
Weak signals
Wild cards | black swan (you don’t know how to predict it cuz you dont know its going to be there until its there)

LIFE IN THE TIMES of SURVEILLANCE CAPITALISM

Big social transformation
How things ended up here
2012: our relationship with internet changed
Olympics london opening ceremony >>> utopia, the promise, the idea of the future internet

The shortest definition of internet: i = tcp / ip + http (late 80s)
Ncsa mosaic: easy access and distribute information
A medium where anyone could make a new point in the net and link it to the net without anyone’s permission
Small pieces loosely joined | david weinberger
The internet expands too fast

Identity in the beggining of the web > anonymity
Tim wu | net neutrality >>> no one has priority
Option “show page source” (right click > inspect ctrl shift I)

Web 2.0
2001-03-04
books:
Here comes everybody
Smarmobs
The wealth of networks

Collaboration - coordination to what, with who, how,


Evgeny Morozov: sceptical to silicon valley, the Internet doesnt exist

The web began dying in 2014, Andre Staltz

Shoshana Zuboff : “the age of surveillance capitalism”

HOW TO MAKE MONEY ON WEB in 2019:
Attention monopoly
Data extractivism
Data markets
Sell data & services
Produce intelligence


ATTENTION ECONOMY
Major turning point >>> when smartphone was created
Time, energy, attention to platforms not existing before
Design decisions > reactions


ENGAGEMENT
Sean Parker >> Napster, Facebook, Spotify (timberlake in “social network”)
Social validation feedback loop

Designing decisions to maximize “addiction”

Infinite scroll

Pull to refresh

9 seconds to decide if u had enough netflix for today

Clickbait
“Fake news”

Extreme personalization >> data shadow (who is that person created by your “choices” who is not exactly you)
The filter bubble > the eco chamber >>> eniological polarisation
microtargeting


DISPOSSESION by SURVEILLANCE

DATA EXTRACTIVISM

MACHINE LEARNING

**WEAK SIGNALS of the week:

ATTENTION PROTECTION;
DISMANTLING FILTER BUBBLES;
CIRCULAR DATA ECONOMY;
THE TRUTH WARS;
REDESIGNING SOCIAL**

********************************************************************************************************

WEEK 2;

DESIGNING FOR THE ANTHROPOCENE

1.THINKING IN GEOLOGICAL SCALES
Time scale
Things exist in much wider time expand
We are incorporating decisions we take today
The geological expand of time
The time of mankind in relation to the time of the earth
When did we start messing things up? Industrial revolution? NO -agricultural revolution
12 thousand years: turning point 1
Appearance of philosophy

1 hundred and 50 years: turning point 2
Workforce artificially developed
COAL
Resources and externalities

July 16th, 1945: something has changed from this
The geological turning point
Radioactivity levels

We are not anymore in the whole scene. We have entered a new period
The idea of the anthropocene: the activity of the human species is the biggest impact on the world, much bigger than any other ecosystem

Capitalocene: the act of every single human equally affecting the line of impact

Tension between nature and culture
The chemical composition of our air is changed
We are manipulating mother nature


2 THE GREAT ACCELERATION
The last 60 years were accelerated
The natural resources are being used in a rate unpresented

Humans in numbers
We are gonna pick at around 11 billion in about 50 years and then start decline
Cuz of development and women rights&education (the more developped a country is the less the rate becomes. The more women have a voice over the subject the less births)

The limits to growth
The resources are not unlimited
We need 3 planets to produce the sources we use

Bruno Latour: “the globe is bigger than the planet”


3 BIOPHISICAL LIMITS
63% of CO2 was made since 1950

The uninhabitable earth by David Wallce Wells
“It is much worse than you think”

CO2 in atmosphere
412,04  today
408.59 a year ago today
250 safe point

NYC is spending $10 billion to protect Manhattan
Bjarke Manhattan project

Greenland ice is melting

Species extinct
Humanity has wiped out 60% of animal populations since 70’s
20% per decade diminishing of insect population
The 6th extinction
The background extinction level 3 or 4 per decade

WHAT'S THE PLAN??

The Paris agreement
By the end of the century: 1,5 to 2*C higher atmosphere temperature since 1970 (today +1,1)
Producing too much CO@ and deforestation led to the no we have to face

Find technology that transforms CO2 in something using less energy


CONFLICTS OF THE ANTHROPOCENE

Plastic is the symbol of this era
Plastic mineral found in pacific coast
Microplastic pollution

Commodities:
water and sand
Food
Refugees

A POLITICS ON A GEOLOGICAL SCALE
The minister for future generations

GEOENGINEERING
https://www.leeds.ac.uk/news/article/3631/geoengineering_our_climate_is_not_a_quick_fix

BEYOND THE ANTHROPOCENTRISM
http://www.perc.org.uk/project_posts/on-androcentrism-anthropocentrism-ppe-student-blogpost/

Donna Haraway “ staying with the trouble”
We are highly depended on other species

**WEAK SIGNALS of the week:

CLIMATE CONSCIENCE;
INTERSPECIES SOLIDARITY;
LONG TERMISM;
CARBON NEUTRAL LIFESTYLES;
FIGHTING ANTHROPOCENE CONFLICTS**

********************************************************************************************************
24 QUESTIONS FOR RADICAL SCENARIO BUILDING by Mariana
********************************************************************************************************

WEEK 3;

LIFE AFTER AI and the END OF WORK

— go deeper with the narrative / design proposal
— name your space of research

What is a job?

the means by which:

-the economy produces goods

-people earn income and develop a sense of purpose and personal identity

labor money-making: the job everyday - afford - is become less important

capital money-making: your house - it earns on itself, it is becoming more and more important, NYC you are using capital to increase capital

share of global wealth 2010-15

both has the same value in the economy

drivers:

palete and the conteiner - cost of labour to produce service - distribution

information economy ( AT&T / Google )

the gig economy - glovo ( you can decide when you are working when you are not working - flexible shape of the market),

Amazon- kiba robots, develop infrastructure, "amazon go" first algorithm based supermarket, air drone  delivery,

human collaborating with the robot are not well paid

Most common job are not highly skilled (map USA, 2014)

the future of employment - automatization of the jobs

//photo

Life after work

John Maynard Keynes,

life is organized between two places work an

Will robots take all our jobs?

Yual Noah Harrari; the useless class- the capital and the labor is expanding

the luddite fallacy - against technology, workers that started being automated, they were out of the job,  coal mine

why l'm sceptical the robots will take all our jobs - yoga teacher, open space for profesion that doesn't exist yet

AI is more than reproduction human capasities, jobs is being automation than before

Alexandra Ocasio Cortez

we do not want robots
wealth and job value on the balance in the past and future?
B-mincome - barcelona level of impact of employment

fully automated luxury communism

What will people do?

-EVERYBODY WILL BE A ARTISTIAN  new reans -utopa vision  / automatization / education structure / weak signals

care economy

coach potato - activity

**Some random notes on AI.**
alexa speaker
mit media lab - gender shades
face ++ - chinees software with huge data base

**"to creative for AI" -** creative cooperation/collaboratoin
- human and non human decision on one creative process in the dycamic system
- google musician AI
- marco klingenman painting,  GIN system, variation of data sets
- holy herton

**WEAK SIGNALS of the week:

TECHBOLOGY FOR EQUALITY
FIGHTING AI BIAS
IMAGINE NEW JOBS
MAKING UBI WORK - HOW NOT TO BECOME COACH POTATOES
HUMAN-MACHINE CREATIVE COLLABORATIONS**

********************************************************************************************************

WEEK 4:

**WEAK SIGNALS of the week:

MAKING WORLD GOVERNANCE;
RURAL FUTURES;
PICK YOUR OWN PASSPORT;
REFUGEE TECH;
WELLFARE STATE 2.0**

********************************************************************************************************

WEEK 5:

Kill the hetereopatriarchy

addressing to priviledge and bias

decoding sexuality

representing races and cultures; philosophies in narratives like: margaretatwood ursula

Invisible Women by Caroline Perez

**WEAK SIGNALS of the week:

NOW HETEROPATRIARCAL INNOVATION;
IMAGINE FUTURES NON WESTERN-CENTRIC;
RECONFICURE YOUR BODY;
GENDER FLUIDITY;
DISRUPT AGEISM**

********************************************************************************************************
