---
layout: single
permalink: /docs/_pages/atlas/development
title: "Atlas of Weak Signals, Development"
excerpt: ""
header:
    image: /assets/images/atlasdevelopment.png

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

---

##SHORT DESCRIPTION, || i.e the concept

In this of the seminar the objective is to obtain knowledge based on harvested trends, and data and collectively built an ontology and concept map, create evolving knowledge base and a navigable repository.
Possible understanding of data visualization.

##DOCUMENTATION, || i.e not sorted notes taken from the lectures

26.03.2019

Semiotics: design philosophy of understanding symbols

Keywords
What we extract out of the data
Automation problems:
Extreme bias, human factor, bugs, miss stuff, Eco chambers, dictionary, recursive/circular automation, shifting responsibility from you to some rules

in this case is super easy to miss the rule's set, decision tree, you cant know about think that you do not know

Everything is Bias

HOW WE TALK ABOUT SYMBOLS, Understanding Semiotics

Concept of rule set (semiotic in design)

---
![](../atlas/assets/images/symbols.png)
---

Concept: context

Object: physiology

Symbol: what I construct to combine concept & object, “universal” agreement on how we call it

method : what tests if concept and object match

Memory and experience

Mapping of concept and object

cat(concept) what this is means to me, conceptual framework

cat(object) physical object, visual representation

cat(symbol)

method - is a way of mapping  concept and object together, we are try to define semiotic landscape,

unflexible connections

zebra is not a horse

Semiotic map

Logical steps to go from one term to the other

Keywords as bridges, jumping points from topic to topic

---
![](../atlas/assets/images/semioticmap.jpg)
---

¿¿¿ATLAS of TRUST???

**homework**
—>semiotic to new topic
—>keywords as bridges
—> continue experiment with sheet and keywords
work on your topic - start doing it by hand




04.04.2019

experiences, intervention, application, instalation - it should connect at least 3 topics (min.)


**presentation context - group work**
Why it is important/ weak signal (statistical reason, references, meat is more important than visual)
How it is affected / fictional future scenario
What are you
What the topic is and how is it connected to the rest topics?
more topics, concreate, conharent, inclusive,
friction in discussion
