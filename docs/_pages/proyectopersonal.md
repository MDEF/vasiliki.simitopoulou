---
layout: single
permalink: /docs/_pages/proyectopersonal/
title: "DESIGN STUDIO"
excerpt: ""
header:
    image: /assets/images/coverproyecto.gif.gif

last_modified_at: 2019-03-20T20:20:03-05:00
toc: true

feature_row:

  - image_path: /assets/images/notebook.png
    alt:
    title: "NOTEBOOK"
    excerpt: ""
    url: "/docs/_pages/proyecto/notebook/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

  - image_path: /assets/images/terms.png
    alt:
    title: "TERMS 1-2-3"
    excerpt: ""
    url: "/docs/_pages/proyecto/proyectoterms/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

  - image_path: /assets/images/sources.png
    alt:
    title: "SOURCES"
    excerpt: ""
    url: "/docs/_pages/proyecto/sources/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

  - image_path: /assets/images/proyectoarticle.png
    alt:
    title: "ARTICLE"
    excerpt: "mdef journal"
    url: "/docs/_pages/proyecto/article/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

  - image_path: /assets/images/proyectovideo.png
    alt:
    title: "FINAL CUT"
    excerpt: "mdef video"
    url: "/docs/_pages/proyecto/video/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

  - image_path: /assets/images/proyectoexhibition.png
    alt:
    title: "EXHIBITION"
    excerpt: "final presentation"
    url: "/docs/_pages/proyecto/exhibition/"
    btn_class: "btn--primary"
    btn_label: "Learn more"

---

##SHORT DESCRIPTION, || i.e the concept

NOTEBOOK: Scatter thoughts, doodles, etc. The 9 month process of my personal project.

TERMS 1-2-3: Research, Design and Development; the aim of the studio is to take research areas of interest and initial project ideas into an advanced concretion point, and execution plan. The studio structure is developed throughout the three terms:

>Research Studio; Analyzing the past. References, state of the art. Identifying areas of interest. ||
Design Studio; Forming the present. Building the foundations. Applying knowledge into practice. Prototyping and experimenting. ||
Development Studio: Defining the future. Establishing roadmaps. Forming partnerships. Testing ideas and prototypes in the real world.

SOURCES: The books, articles, papers, videos, projects, etc used as inspiration and references for the studio.

ARTICLE: The MDEF journal, where all the information around the final intervention are collected and presented.

VIDEO: A short visual of what my project intervention is about, why it is important and relevant to the community; the promo cut of the project.

EXHIBITION: Design, presentation, material; the final presentation of the project.

---

{% include feature_row %}
