#07. The way things work

Week 7/12: more than half way there and it’s time to interact with some everyday objects, tear them apart, understand their structure and hack them into new hybrid objects.

![](assets/tools.gif)


#Day 1: Disassembling#

Ok; a coffee machine is much more simple than I thought and a plain plotter printer is much more complex. Look at all these small pieces there (I hope they won’t ask us to put them back together! I have no idea what goes where). What caught my attention though was the relatively old phone that was left there wide open. *Is it really that complex to recreate sound?*

![](assets/PHOTO 1.jpg)


Now that we have the ingredients let’s fresh up our memory; we need recipes -in that case arduino scripts. *the horror* my code skills are so limited and the first and only touch with arduino was on the very first week, during precourse. I kind of remember how to use a button and light an led, but how is this going to be of any use? Well it's a start..

#Day2: Coding | sensor making#

So if we know what we want to create, how do we code the system behind it? Do we start writing lines of code? Well I am lucky because this usually not the case; process flow charts can help us visualise and understand what we want to create. You can use this diagram with specific symbols in order to describe the process, represent the algorithm and illustrate a solution model to the given problem.

![](assets/flow.jpg)


Eg. Switch on the heater, if the temperature is lower than 20C, using a sensor detecting people in a room

![](assets/flow eg.jpg)

So now that we kind of understand the process it's time to choose the sensor we are going to use. There is a huge variety of sensors out there readymade and you can use them in a bunch of possible projects.

![](assets/sensors.jpg)

*GROUP PROJECT TIME
Let’s take a speaker (usually used as an output) and turn it into an input sensor to turn an LED on

![](assets/group proj 1.jpg)

Well that was easy. One wire to the ground, one to the power (for both the speaker and the LED) and then the speaker is acting like a button. DONE*

#Day3: Network-ing | Prototyping#

We use WiFi everyday and we take it for granted but how does it really work? **WiFi is an abbreviated term that stands for -according to the most widely accepted definition in the tech community- Wireless Fidelity** WiFi is a technology that uses radio waves to provide network connectivity. A WiFi connection is established using a wireless adapter to create hotspots - areas in the vicinity of a wireless router that are connected to the network and allow users to access internet services. Once configured, WiFi provides wireless connectivity to your devices by emitting frequencies between 2.4GHz - 5GHz, based on the amount of data on the network.
Wireless technology has widely spread lately and you can get connected almost anywhere; at home, at work, in libraries, schools, airports, hotels and even in some restaurants.
Wireless networking is known as WiFi or 802.11 networking as it covers the IEEE 802.11 technologies. The major advantage of WiFi is that it is compatible with almost every operating system, game device, and advanced printer. Like mobile phones, a WiFi network makes use of radio waves to transmit information across a network. The computer should include a wireless adapter that will translate data sent into a radio signal. This same signal will be transmitted, via an antenna, to a decoder known as the router. Once decoded, the data will be sent to the Internet through a wired Ethernet connection. As the wireless network works as a two-way traffic, the data received from the internet will also pass through the router to be coded into a radio signal that will be received by the computer's wireless adapter.

So can we create a wireless network for the purposes of our projects?

*GROUP PROJECT TIME 2
Let’s create a musical instrument; a synthesizer to be precise. A christmas-y synthesizer to be even more precise.
I am really happy and thankful that I worked on this with Gabor, as he did all the coding part and I just played around with the wires and leds and buttons on arduino; on 2 arduinos.

CHRISTMAS ARDUINO

It took a lot of effort and many many tests but the result was so amazing. 2 arduino boards, 3 plates, 5 leds, 5 buttons, 1 speaker and almost 50 wires and resistors; this a synthesizer ladies and gentlemen
SYNTHESIZER VIDEO

Each button is connected to an led and works as a keyboard note. Next step: make it more simple*

#Day4: Prototyping vol. II#

**GROUP PROJECT TIME 3
So the our goal was pretty obvious for today. This synthesizer should be simplified. So the approach was to reduce our synthesiser to one Arduino and one breadboard. For that, Gabor started the code from 0 and used a chart to generate the different tones with a delay on the digital output signal.**
*That is part of the code used by Gabor*

CODE

Then the struggle was to put everything on 1 arduino and 1 plate.
*This is how it looks*

![](assets/code.jpg)

*...and how it sounds*

https://vimeo.com/302610644

#Day5: Prototyping vol. III#

Last day of the week and now we have to take it to the next level, connecting all the pieces of knowledge we gained through this module. So we recreated a phone that gets calls by… a plant! Check it out..

https://vimeo.com/302679136

The first task was to transfer the project from arduino to ESP8266 chip which proved to be quite a challenge as the ESP does not have the same sound processor although it is based on the design of arduino. To solve that Gabor used the itone() function, which uses the timer to create sounds in specific frequencies. To make the project work in the created environment the MGTT protocol was added, so the incoming messages in the range of 0 to 255 were remapped to 2000 to 2000 as a value of the frequency.
So an “input” could now call the phone. With a Rasberry Pi we set up an MQTT server so that we could exchange signal among us. Eg.by touching a plant you could change the pitch to the phone or open the door and to start a compressor which led to an exploding balloon.

And this is how another crazy and productive and mindblowing week ends..
