#11. From Bits to Atoms


One week to the last one of this term and we have the opportunity to get a sneak peek to what we are going to face during the upcoming months -as part of the fab lab academy. The fab lab philosophy is pretty clear: rethink how we make things and why, and generate active and practical conversations through projects and prototypes that become manifests itself; make anything, anywhere with creativity and imagination, using tools and tech mixed with new techniques in order to generate new appropriate solutions.

In this intensive week we will have an introduction to the environment of the Fab Lab with the already known learning-by-doing approach. We are asked to design, develop and fabricate a fully operative machine, using 3d modelling to visualise and digital fabrication to produce.

So here we go; 35 hours, 5 teams, 5 machines; let’s go for this extra 5% of the evaluation [if it doesn’t explode XD ]. As Christmas is just round the corner we decided to do something that, apart from following the syllabus, would give some christmas spirit; what better than a machine that makes colourful and fluffy pom-poms !?

MACHINE DRAFT

First we tried to analyse what kind of moves we need the machine to make, what shape of components we may need to design and what type of inputs we will need to have in order to control the machine. What may seemed easy at first, proved to be quite a challenge. For sure it is a thing to make a box look nice and a completely different thing to make it do what you want it to do. You can design parts that fit on paper (or on rhino to be more accurate) but then something “magical” happens when you actually send the design to the lasercut or the 3dprinter; let aside the part where the electronics take control. *dear arduino I love you and I hate you simultaneously* It is fascinating how many things you can control with this small panel and a couple (or maybe a dozen, or even more!) wires. At the same time it is highly complicated and things can go wrong without you even knowing why -thus how to fix them.

Well, at the end everything worked out perfectly and we now have a machine that can make all the hard work (of making pom-poms) for us -and plays some christmas music as well
#here you can find the deposit of the group project, with the process we followed, photos and videos of the prototypes and the final machine, and all media related : https://mdef.gitlab.io/dj-pom-pom/
