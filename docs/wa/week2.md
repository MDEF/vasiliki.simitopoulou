#02. BIOLOGY ZERO

So here we are. Second week of the masters. The daily schedule is full. 9.30 to 18.30 every day. And for some strange reason we have to study biology this week. What? Why? And so on… so many questions that I didn’t seem to be able to answer. In a way I was kind of reluctant because biology was of my least favorite subjects through high school and I always said “oh come on, I will never need this”.
But I was wrong.. So very wrong..
There are so many fields related and affected by biology that you cannot say that you will never find something interesting or useful. It’s a matter of perspective and scale. You can study from bacteria to fossils of big extinct creatures and from molecules to cells to living organisms and still be under the umbrella of biology.

![](assets/w02_1.jpg)

Basically everything about life. And there comes Dr Nuria Conde Pueyo -or just Nuria- trying to explain that what we define as alive is not that simple. I mean you can find a definition by just searching on Google but that is not quite accurate. For sure terms like metabolism, growth, reproduction, death, response, movement keep pop-ing up but the definition of them varies as well.
Truth is that life cannot be defined in terms of a list of distinctive properties that distinguish living from non-living systems, as they often overlap and each attempt at a definition are inextricably linked to a theory from which it derives its meaning (*Benner 2010*). There have been three main philosophical approaches to the problem of defining life that remain relevant today: Aristotle's view of life as animation, a fundamental, irreducible property of nature; Descartes's view of life as mechanism; and Kant's view of life as organization, to which we need to add Darwin's concept of variation and evolution through natural selection (*Gayon 2010; Morange 2008*). In addition we may add the idea of defining life as an emergent property of particular kinds of complex systems (*Weber 2010*).

**So what’s life? Is it a philosophical question or the fundamental in order to understand the matter that envelop us?
If we go back in time -and I mean quite a lot back- we can see that the first known indication of life is not even close to similar to what someone can define as living creature. Yet is our one and only common ancestor. It took billions of years and processes that we can only partially explain to get to where we are today.**

![](assets/w02_2.jpg)

So many questions rise by this travel back in time. And not only in attempt to explain what happened but also in a desperate shot to predict the future.
*eg How did the first multi-cellular organisms “decided” to form?
How did we end being as we are today? How did evolution occur? *
**https://www.youtube.com/watch?v=GhHOjC4oxh8**

And that’s the point you actually got me. This almost unexplored path around evolution intrigued me.

 **I’ll get back to it later**

Back to the week reality. So, for the next 5 days our daily routine includes: 4 hours of theory about the basics of almost everything regarding biology and 4 hours of experiments in a diy bio-lab. We go from bio-engineering to biochemistry and from studying microorganisms to understanding how our metabolism works and observing parts of our cells.
Although the input of information was almost too much to handle, for me the common ground and what seems really interesting is to see how many grey areas there are when we talk about aspects related to modern era biology
*eg Coding life (create something from nothing in a lab) and genetic modification are not that publicly accepted as immoral and unnatural, even though we have been “choosing” which way evolution should go from the beginning of (human)times. [agriculture, race breeding etc]*

![](assets/w02_3.jpg)

If we could modify the evolutionary path by just choosing and mixing before, what about now that we can actually intervene on the DNA? Is everything that we can imagine possible? Are the marvel’s X-Men close to become reality? Can mutan living organisms be created?
*well..it seems they already exist and it’s not sci-fi anymore. Not human beings -yet- but there is a variety of new gmo(genetically modified organisms) from the “cute” Glofish - a fluorescent zebrafish created almost 20 years ago by Dr Zhiyuan Gong at NU Singapore- to the more disturbing, to my eyes at least, Vacanti mouse - a laboratory mouse that had what looked like a human ear grown on its back created in 1995 at the University of Massachusetts by Dr Charles Vacanti.*

So to understand the possibilities given by this “technology” we have to be able to understand how things work and know where we want to make an interference. For that purpose we have to dive into cells, proteins, genes, etc. All these little things existing inside everything, moving, reproducing and yet remain invisible to the naked eye.
From ancient times, man has wanted to see things far smaller than could be perceived with the naked eye. Although the first use of a lens is a bit of a mystery, it’s now believed that use of lenses is more modern than previously thought. However, it has been known for over 2000 years that glass bends light. In the 2nd Century BC, Claudius Ptolemy described a stick appearing to bend in a pool of water, and accurately recorded the angles to within half a degree. He then very accurately calculated the refraction constant of water. The early simple “microscopes” which were only magnifying glasses had one power, usually about 6x – 10x. One thing that was very common and interesting to look at, were fleas and other tiny insects, hence these early magnifiers called “flea glasses”. Sometime, during the 1590’s, two Dutch spectacle makers, Zaccharias Janssen and his father Hans started experimenting with these lenses. The object near the end of the tube appeared to be greatly enlarged, much larger than any simple magnifying glass could achieve by itself! Their first microscopes were more of a novelty than a scientific tool since maximum magnification was only around 9X and the images were somewhat blurry. Although no Jansen microscopes survived, an instrument made for Dutch royalty was described as being composed of “3 sliding tubes, measuring 18 inches long when fully extended, and two inches in diameter”. The microscope was said to have a magnification of 3x when fully closed, and 9x when fully extended. It was Antony Van Leeuwenhoek (1632-1723), a Dutch draper and scientist, and one of the pioneers of microscopy who in the late 17th century became the first man to make and use a real microscope.
Thanks to Antony Van Leeuwenhoek we can now observe thing like that:

![](assets/w02_4.jpg)

Actually I have to admit that bio lab was quite fun. Creating bacteria using things one can have in the kitchen, using microscopes to monitor them, etc.

![](assets/w02_video.mp4)

Oh well..diy labs are fun but I saw and experienced on the last day of this week is beyond imagination and I can hardly describe it. Field trip to PRBB (Barcelona Biomedical Research Park) on a Friday morning? Yes please.

![](assets/w02_5.jpg)

The so-called “factory of the future” with an accumulated R&D budget of approximately 90m€ / year and 1,500 people working with cutting-edge scientific equipment promises to explore the most relevant questions in life sciences and biomedicine of the modern era.
The one-of-a-kind hub is housed in an exceptional 55,000m2 building located just on the seafront of Barcelona, with an avant-garde elliptical and conical shaped design by the architects Manel Brullet and Albert de Pineda.

![](assets/w02_6.jpg)

![](assets/w02_7.jpg)

Nice place to work… Workspace is really important for motivation and creativity and for sure PRBB is on the right track with both its interior and exterior spaces.

![](assets/w02_8.jpg)

To sum up this week in a few sentences.
*You don’t know what you don’t know and you should always be prepared to know something more.

Biology is for sure useful.

Labs may require huge budgets and fancy equipment but there is an alternative which works quite the same (diy). Eg. you can spend a few million euros on a spectrophotometer (new words there) or you can make yourself a gadget that can do pretty much the same.

There are online libraries, networks, guides willing to help at every step.*

[References: http://www.thelabrat.com/
https://www.hackteria.org/
https://www.ginkgobioworks.com/ ]

*Biology can be fun.*

End of week 2.
Tired but delighted. Yet still there is one final step before we leave biology rest for a while. THE ASSIGNMENT.

**”Find what interests you in the field of biology, form a hypothesis and describe the research methodology we would follow”**

While trying to process the information presented to us and form in my mind the outline of the assignment that we were given, I went through my notes seeking for any spontaneous thoughts I wrote down during lectures or experiments.
In every single page I have somehow highlighted things regarding evolution. I already mentioned above that I find evolution theory quite intriguing. What interests me the most is how we got here. And if we can go back and get back here again. So my hypothesis could be: “if we recreate the conditions of billions of years ago and from that point “force” our ancestors (more likely cyanobacteria) to evolve, we can create a parallel evolutionary path with descendants that are similar in way and completely different in another.”

*It may sound like a sci-fi movie or something and maybe the initial thought was inspired by the DC universe and the Flash, who, going back in time and changing a single fact, created a whole new timeline*
So the methodology should look like that:

bulletpoint1: find fossils of our great grand ancestors

bulletpoint2: study them in order to be able to recreate their DNA

bulletpoint3: search for as many references as possible regarding the environment in which this ancestor lived and evolved

bulletpoint4: condact experiments with some samples to get the conditions right

bulletpoint5: create a sterilized booth which is going to house the new “old” environment

bulletpoint6: condact the experiment

I expect the results to be according to the hypothesis because “forced” and “controlled” evolution will most likely have different outcomes than the ones we already have. Time is an essential part of the process. It took billions of years to get from cyanobacteria to this diversity. Furthermore evolution includes some errors (mutations) happening by chance, which you cannot actually control.

In any case my ulterior motive is to see if by going back in time we can find solutions for the future. For example maybe we can find a way to consume less, or our cells not to get affected by some viruses that are now fatal.

*I only hope the parallel evolution timeline is not as messed up as flash timeline was*/
